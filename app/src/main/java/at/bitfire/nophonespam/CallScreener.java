package at.bitfire.nophonespam;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.telecom.Call.Details;
import android.telecom.CallScreeningService;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import at.bitfire.nophonespam.model.CallRecord;
import at.bitfire.nophonespam.model.CallRecordDbHelper;
import at.bitfire.nophonespam.model.DbHelper;
import at.bitfire.nophonespam.model.Number;

import java.util.Date;

@RequiresApi(api = VERSION_CODES.Q)
public class CallScreener extends CallScreeningService {
    private static final int NOTIFY_REJECTED = 0;
    private static final String TAG = "NoPhoneSpam";

    @Override
    public void onScreenCall(Details details) {
        if (details.getCallDirection() != Details.DIRECTION_INCOMING) {
            return;
        }

        String phoneNum = details.getHandle().getSchemeSpecificPart();
        Log.i(TAG, "Got number: " + phoneNum);

        String incomingNumber = details.getHandle().getSchemeSpecificPart();
        Settings settings = new Settings(this);
        if (TextUtils.isEmpty(incomingNumber)) {
            // private number (no caller ID)
            if (settings.blockHiddenNumbers()) {
                rejectCall(details, incomingNumber, null);
                showNotification(null);
            }
        } else {
            //TODO check if number is from contacts

            DbHelper dbHelper = new DbHelper(this);
            try {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                Cursor c =
                    db.query(Number._TABLE, null, "? LIKE " + Number.NUMBER, new String[]{incomingNumber}, null, null,
                        null);
                boolean inList = c.moveToNext();
                if (inList && !settings.whitelist()) {
                    ContentValues values = new ContentValues();
                    DatabaseUtils.cursorRowToContentValues(c, values);
                    Number number = Number.fromValues(values);

                    rejectCall(details, incomingNumber, number);
                    showNotification(number);

                    values.clear();
                    values.put(Number.LAST_CALL, System.currentTimeMillis());
                    values.put(Number.TIMES_CALLED, number.timesCalled + 1);
                    db.update(Number._TABLE, values, Number.NUMBER + "=?", new String[]{number.number});

                    BlacklistObserver.notifyUpdated();

                } else if (!inList && settings.whitelist()) {
                    Number number = new Number();
                    number.number = incomingNumber;
                    number.name = getResources().getString(R.string.receiver_notify_unknown_caller);

                    rejectCall(details, incomingNumber, number);
                    showNotification(number);
                    BlacklistObserver.notifyUpdated();
                }
                c.close();
            } finally {
                dbHelper.close();
            }
        }
    }

    private void rejectCall(Details details, String incomingNumber, Number number) {
        CallResponse response = new CallResponse.Builder()
            .setDisallowCall(true)
            .setRejectCall(true)
            .setSilenceCall(true)
            .setSkipCallLog(false) // doesn't work
            .setSkipNotification(false)
            .build();
        respondToCall(details, response);
        Log.i(TAG, "Rejected call " + incomingNumber);

        recordCall(incomingNumber, number);
    }

    private void recordCall(String incomingNumber, Number number) {
        try (CallRecordDbHelper dbHelper = new CallRecordDbHelper(this);
             SQLiteDatabase db = dbHelper.getWritableDatabase()) {

            String name = number != null ? number.name : "private number";
            ContentValues values = new ContentValues();
            values.put(CallRecord.NUMBER, incomingNumber);
            values.put(CallRecord.NUMBER_NAME, name);
            values.put(CallRecord.CALL_TIME, (int) ((new Date().getTime() / 1000)));

            db.insert(CallRecord._TABLE, null, values);
        }
    }

    private void showNotification(Number number) {
        Settings settings = new Settings(this);
        if (settings.showNotifications()) {

            if (Build.VERSION.SDK_INT >= 26) {
                NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationChannel channel = new NotificationChannel(
                    "default", getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT
                );
                channel.setDescription(getString(R.string.receiver_notify_call_rejected));
                notificationManager.createNotificationChannel(channel);
            }

            Notification notify = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher_small)
                .setContentTitle(getString(R.string.receiver_notify_call_rejected))
                .setContentText(number != null ? (number.name != null ? number.name : number.number)
                    : getString(R.string.receiver_notify_private_number))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_CALL)
                .setShowWhen(true)
                .setAutoCancel(true)
                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, BlacklistActivity.class),
                    PendingIntent.FLAG_UPDATE_CURRENT))
                .addPerson("tel:" + number)
                .setGroup("rejected")
                .setChannelId("default")
                .setGroupSummary(
                    true) /* swy: fix notifications not appearing on kitkat: https://stackoverflow.com/a/37070917/674685 */
                .build();

            String tag = number != null ? number.number : "private";
            NotificationManagerCompat.from(this).notify(tag, NOTIFY_REJECTED, notify);
        }

    }
}
