package at.bitfire.nophonespam;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.ContentValues;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import at.bitfire.nophonespam.model.CallRecord;
import at.bitfire.nophonespam.model.CallRecordDbHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CallRecordActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<CallRecord>> {
    private static final String TAG = "NoPhoneSpam";
    private ListView records;
    private ArrayAdapter<CallRecord> recordAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_record);

        records = findViewById(R.id.call_records);
        recordAdapter = new CallRecordAdapter(this);
        records.setAdapter(recordAdapter);

        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<List<CallRecord>> onCreateLoader(int id, Bundle args) {
        return new CallRecordLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<List<CallRecord>> loader, List<CallRecord> records) {
        recordAdapter.clear();
        recordAdapter.addAll(records);
    }

    @Override
    public void onLoaderReset(Loader<List<CallRecord>> loader) {
        recordAdapter.clear();
    }

    private static class CallRecordAdapter extends ArrayAdapter<CallRecord> {
        public CallRecordAdapter(Context context) {
            super(context, R.layout.call_record_item);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null) {
                view = View.inflate(getContext(), R.layout.call_record_item, null);
            }

            CallRecord record = getItem(position);

            TextView tv = view.findViewById(R.id.call_record_number);
            tv.setText(record.getNumber());

            tv = view.findViewById(R.id.call_record_number_name);
            tv.setText(record.getNumberName());

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss z");
            String callTime = sdf.format(new Date(record.getCallTime() * 1000));
            tv = view.findViewById(R.id.call_record_stats);
            tv.setText(callTime);

            return view;
        }
    }

    private static List<CallRecord> loadRecords(Context context) {
        Log.i(TAG, "loading db");
        List<CallRecord> records = new ArrayList<>();

        try (
            CallRecordDbHelper dbHelper = new CallRecordDbHelper(context);
            SQLiteDatabase db = dbHelper.getReadableDatabase()
        ) {
            Cursor c = db.query(CallRecord._TABLE, null, null, null, null, null, CallRecord.CALL_TIME + " DESC");
            while (c.moveToNext()) {
                ContentValues values = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(c, values);
                records.add(CallRecord.fromValues(values));
            }
        }
        return records;
    }

    protected static class CallRecordLoader extends AsyncTaskLoader<List<CallRecord>> {
        public CallRecordLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            forceLoad();
        }

        @Override
        public List<CallRecord> loadInBackground() {
            return loadRecords(getContext());
        }
    }
}
