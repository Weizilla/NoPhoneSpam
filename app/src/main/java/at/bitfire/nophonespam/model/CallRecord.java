package at.bitfire.nophonespam.model;

import android.content.ContentValues;

public class CallRecord {
    public static final String _TABLE = "CallRecord";
    public static final String NUMBER_NAME = "numberName";
    public static final String NUMBER = "number";
    public static final String CALL_TIME = "callTime";

    private String numberName;
    private String number;
    private long callTime;

    public String getNumberName() {
        return numberName;
    }

    public void setNumberName(String numberName) {
        this.numberName = numberName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getCallTime() {
        return callTime;
    }

    public void setCallTime(long callTime) {
        this.callTime = callTime;
    }

    public static CallRecord fromValues(ContentValues values) {
        CallRecord record = new CallRecord();
        record.number = values.getAsString(NUMBER);
        record.numberName = values.getAsString(NUMBER_NAME);
        record.callTime = values.getAsLong(CALL_TIME);
        return record;
    }
}
