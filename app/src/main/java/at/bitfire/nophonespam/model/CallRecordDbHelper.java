package at.bitfire.nophonespam.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CallRecordDbHelper extends SQLiteOpenHelper {
    public CallRecordDbHelper(Context context) {
        super(context, "callRecordDatabase", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + CallRecord._TABLE + "(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            CallRecord.NUMBER_NAME + " TEXT NOT NULL, " +
            CallRecord.NUMBER + " TEXT NOT NULL, " +
            CallRecord.CALL_TIME + " INTEGER NOT NULL" +
            ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
